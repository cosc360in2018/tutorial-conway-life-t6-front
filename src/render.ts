import Life from "./gameOfLife"
import { GameConfig } from "./gameConfig"
import * as d3 from "d3"

let foo = "bar"

let life:Life = new Life(0,0)

function create(config:GameConfig) {
    console.log(config.w)
    life = new Life(config.w, config.h)
    render()
}

const game = d3.select("#game").append("g")
const cellSize = 20

export {
    getLife as getLife,
    create as create,
    render as render
}

function getLife() {
    return life
}

function render() {
    console.log("rendering")
    
    let update = game.selectAll("rect").data(life.board)
      
    update.attr("class", (d) => {
        return d ? "cell alive" : "cell"            
    })
      
    update.enter()
        .append("rect")
        .attr("class", (d) => {
            return d ? "cell alive" : "cell"            
        })
        .attr("x", (val,i) => {
          return life.x(i) * cellSize
        })
        .attr("y", (val,i) => {
          return life.y(i) * cellSize
        })
        .attr("width", cellSize)
        .attr("height", cellSize)
        .on("click", (d,i) => {
            let x = life.x(i)
            let y = life.y(i)
            life.toggleCell(x,y)
            render()
        })
}