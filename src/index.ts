import Vue from "vue";
import { render, getLife } from "./render"
import { ConfigView, GameConfig } from "./gameConfig"

let config:GameConfig = {
    w: 40,
    h: 20,
    started: false,
    created: false,
    period: 500
}

let v = new Vue({
    el: "#app",
    template: `
      <div>
        <ConfigView :config="config"></ConfigView>    
      </div>
    `,
    data: {
        config: config
    },
    components: {
        ConfigView
    }
})


console.log("loaded vue")

console.log(v)


document.getElementById("step")!.addEventListener("click", () => {
    let l = getLife()
    if (l) l.stepGame()
    render()
})