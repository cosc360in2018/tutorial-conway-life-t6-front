import Vue from "vue"
import Component from 'vue-class-component'
import { create, render, getLife } from './render'

export { 
    GameConfig as GameConfig,
    ConfigView as ConfigView
}

interface GameConfig {
    w:number 
    h:number 
    started:boolean 
    created:boolean
    period:number 
}

@Component({
    props: {
        config: Object as () => GameConfig
    },
    template: `
      <div>
        <div class="form-row align-items-center">
          <div class="col">
            <div class="input-group">        
                <div class="input-group-prepend">
                  <span class="input-group-text" >Cols</span>
                </div>
                <input type="number" class="form-control" v-model:value="config.w" />
            </div>  
          </div>
          <div class="col">
            <div class="input-group">        
                <div class="input-group-prepend">
                  <span class="input-group-text" >Rows</span>
                </div>
                <input type="number" class="form-control" v-model:value="config.h" />
            </div>  
          </div>
          <div class="col">
            <button class="btn btn-primary mb-2" v-bind:disabled="config.created" v-on:click="create">Create</button>
          </div>
          <div class="col">
            <div class="input-group">        
                <div class="input-group-prepend">
                  <span class="input-group-text" >ms</span>
                </div>
                <input type="number" class="form-control" v-model:value="config.period" />
            </div>  
          </div>
          <div class="col">
            <button class="btn btn-primary mb-2" v-bind:disabled="!config.created" v-on:click="toggleTimer">
            {{ config.started ? "Stop" : "Start" }}
            </button>
          </div>          
        </div>
      </div>
    `
})
class ConfigView extends Vue {

    config!: GameConfig

    constructor() {
        super()
        console.log("a new config view was placed")
    }

    create() {
        create(this.config)
        render()
        this.config.created = true
    }


    intervalId?: number

    toggleTimer() {
        if (this.config.started) {
            clearInterval(this.intervalId)
            this.config.started = false
        } else {
            this.intervalId = setInterval(() => {
                let l = getLife()
                if (l) l.stepGame()
                render()
            }, this.config.period)
            this.config.started = true
        }
    }
    
    
}