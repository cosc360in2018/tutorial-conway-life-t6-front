"use strict";
/// <reference types="d3" />
var life = new Life();
var game = d3.select("#game").append("g");
var cellSize = 20;
function render() {
    console.log("rendering");
    var update = game.selectAll("rect").data(life.board);
    update.attr("class", function (d) {
        return d ? "cell alive" : "cell";
    });
    update.enter()
        .append("rect")
        .attr("class", function (d) {
        return d ? "cell alive" : "cell";
    })
        .attr("x", function (val, i) {
        return life.x(i) * cellSize;
    })
        .attr("y", function (val, i) {
        return life.y(i) * cellSize;
    })
        .attr("width", cellSize)
        .attr("height", cellSize)
        .on("click", function (d, i) {
        var x = life.x(i);
        var y = life.y(i);
        life.toggleCell(x, y);
        render();
    });
}
//# sourceMappingURL=render.js.map